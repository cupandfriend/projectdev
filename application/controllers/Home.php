<?php
class Home extends CI_Controller{
    
	function __construct(){
        
		parent::__construct();
		$this->load->model('m_tulisan');
		$this->load->model('m_galeri');
		$this->load->model('m_pengumuman');
		$this->load->model('m_kontakkami');
		$this->load->model('m_agenda');
		$this->load->model('m_files');
		$this->load->model('m_slide');
		$this->load->model('m_baner');
		$this->load->model('m_guru');
		$this->load->model('m_pengunjung');
		$this->m_pengunjung->count_visitor();
        
	}
    
	function index(){
        
            $this->data['main_view']   = 'depan/v_home';
        $this->data['kontakkami']=$this->m_kontakkami->get_kontakkami_home();
			$this->data['berita']=$this->m_tulisan->get_berita_home();
			$this->data['pengumuman']=$this->m_pengumuman->get_pengumuman_home();
			$this->data['agenda']=$this->m_agenda->get_agenda_home();
			$this->data['guru']=$this->m_guru->get_pegawai_home();
			$this->data['baner']=$this->m_baner->get_baner_home();
$this->data['slide']=$this->m_slide->get_slide_home();
			$this->data['tot_guru']=$this->db->get('tbl_pegawai')->num_rows();
			$this->data['tot_siswa']=$this->db->get('tbl_rakyat')->num_rows();
			$this->data['tot_files']=$this->db->get('tbl_files')->num_rows();
			$this->data['tot_agenda']=$this->db->get('tbl_agenda')->num_rows();
        $this->data['title']  = 'Beranda';
			$this->load->view('theme/template',$this->data);
	}

}

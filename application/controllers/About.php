<?php
class About extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pengunjung');
		$this->load->model('m_tentangkami');
		$this->load->model('m_kontakkami');
		$this->m_pengunjung->count_visitor();
	}
	function index(){
        
        $this->data['main_view']   = 'depan/v_about';
        $this->data['tentangkami']=$this->m_tentangkami->get_tentangkami_home();
            $this->data['kontakkami']=$this->m_kontakkami->get_kontakkami_home();
        
		$this->data['tot_guru']=$this->db->get('tbl_pegawai')->num_rows();
		$this->data['tot_siswa']=$this->db->get('tbl_rakyat')->num_rows();
		$this->data['tot_files']=$this->db->get('tbl_files')->num_rows();
		$this->data['tot_agenda']=$this->db->get('tbl_agenda')->num_rows();
        $this->data['title']  = 'Tentang Kami';
        $this->load->view('theme/template',$this->data);
		
	}
}

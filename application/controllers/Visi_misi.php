<?php
class Visi_misi extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pengunjung');
		$this->load->model('m_kontakkami');
		$this->load->model('m_visimisi');
		$this->m_pengunjung->count_visitor();
	}
	function index(){
        
        $this->data['main_view']   = 'depan/v_visimisi';
        $this->data['kontakkami']=$this->m_kontakkami->get_kontakkami_home();
        $this->data['visimisi']=$this->m_visimisi->get_visimisi_home();
		$this->data['tot_guru']=$this->db->get('tbl_pegawai')->num_rows();
		$this->data['tot_siswa']=$this->db->get('tbl_rakyat')->num_rows();
		$this->data['tot_files']=$this->db->get('tbl_files')->num_rows();
		$this->data['tot_agenda']=$this->db->get('tbl_agenda')->num_rows();
		$this->data['title']  = 'Visi - Misi';
        $this->load->view('theme/template',$this->data);
		
	}
	
}

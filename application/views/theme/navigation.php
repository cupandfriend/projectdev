<div id="sticky-header" class="main-header-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="header_wrap d-flex justify-content-between align-items-center">
                                <div class="header_left">
                                    <div class="logo">
                                        <a href="<?php echo base_url('');?>">
                                            <img src="<?php echo base_url('');?>style/img/halmah.png" alt="" height="100px" width="100%">
                                        </a>
                                    </div>
                                </div><h1 style="font-weight: bold; text-align: left;">DISKOMINFO</h1>
                                <div class="header_right d-flex align-items-center">
                                    <div class="main-menu  d-none d-lg-block">
                                        <nav>
                                            <ul id="navigation">
                                                <li><a  href="<?php echo base_url('home');?>">Beranda</a></li>                                                
                                                <li><a href="#">Profile <i class="ti-angle-down"></i></a>
                                                    <ul class="submenu">
                                                        <li><a href="<?php echo site_url('about');?>">Tentang DISKOMINFO</a></li>
                                                        
                                                        <li><a href="<?php echo site_url('visi_misi');?>">Visi Misi</a></li>
                                                        <li><a href="<?php echo site_url('contact');?>">Kontak Kami</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Berita <i class="ti-angle-down"></i></a>
                                                    <ul class="submenu">
                                                        <li><a href="<?php echo site_url('pengumuman');?>">Pengumuman</a></li>
                                                        <li><a href="<?php echo site_url('blog');?>">blog</a></li>
                                                        <li><a href="<?php echo site_url('agenda');?>">Agenda</a></li>
                                                        
                                                    </ul>
                                                </li>
                                                
                                                <li><a href="#">Dokumentasi <i class="ti-angle-down"></i></a>
                                                    <ul class="submenu">
                                                        <li><a href="<?php echo site_url('galeri');?>">Galeri</a></li>
                                                        
                                                        <li><a href="<?php echo site_url('video');?>">Video</a></li>
                                                        
                                                    </ul>
                                                </li>
                                                <li><a href="<?php echo site_url('download');?>">Peraturan</a></li>
                                                
                                                
                                            <li>
                                            <form action="<?php echo site_url('blog/search');?>" method="get">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" name="keyword" class="form-control" placeholder='Search '
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'Search'" autocomplete="off" required>
                                        <div class="input-group-append">
                                            <button class="btn" type="submit"><i class="ti-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="modal fade" id="ModalSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 10000;">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">  
                <form action="<?php echo site_url('blog/search');?>" method="GET">
                    <div class="input-group">
                      <input type="text" name="search_query" class="form-control input-search" style="height: 40px;" placeholder="Cari..." required>
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="submit" style="height: 40px;background-color: #ccc;"><span class="fa fa-search"></span></button>
                      </span>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
                                        </nav>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
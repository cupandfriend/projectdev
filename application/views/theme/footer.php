<footer class="footer">
        <div class="footer_top">
            <div class="container">
                
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Tentang Kami
                            </h3>
                            <img width="150" src="<?php echo base_url('');?>style/img/halmah.png" class="rounded" alt="Muris Studio">
                        </div>
                    </div><?php foreach ($kontakkami->result() as $row) : ?>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Peta
                            </h3>
                            <iframe src="<?php echo $row->kontakkami_map;?>" width="300" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                   
                   &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Kontak Kami
                            </h3>
                            
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                
                                <p><?php echo $row->kontakkami_jl;?></p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                
                                <p><?php echo $row->kontakkami_hp;?></p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                
                                <p><?php echo $row->kontakkami_email;?></p>
                            </div>
                        </div>
                        </div>
                    </div> <div class="d-flex justify-content-start mt-4">
                    <a class="btn btn-outline-light rounded-circle text-center mr-2 px-0" style="width: 38px; height: 38px;" href="<?php echo $row->kontakkami_twiter;?>"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-outline-light rounded-circle text-center mr-2 px-0" style="width: 38px; height: 38px;" href="<?php echo $row->kontakkami_facebook;?>"><i class="fab fa-facebook-f"></i></a>
                    
                    <a class="btn btn-outline-light rounded-circle text-center mr-2 px-0" style="width: 38px; height: 38px;" href="<?php echo $row->kontakkami_instagram;?>"><i class="fab fa-instagram"></i></a>
                    <a class="btn btn-outline-light rounded-circle text-center mr-2 px-0" style="width: 38px; height: 38px;" href="<?php echo $row->kontakkami_youtube;?>"><i class="fab fa-youtube"></i></a>
                </div><?php endforeach;?>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> <a href="<?php echo base_url();?>" target="_blank">HALMAHERA TIMUR</a> </p>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end  -->


    <!-- JS here -->
    <script src="<?php echo base_url('');?>style/js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/popper.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/ajax-form.js"></script>
    <script src="<?php echo base_url('');?>style/js/waypoints.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/scrollIt.js"></script>
    <script src="<?php echo base_url('');?>style/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/wow.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/nice-select.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/jquery.slicknav.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/plugins.js"></script>
    <script src="<?php echo base_url('');?>style/js/gijgo.min.js"></script>
<script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
<script async src="https://app.cuacalab.id/js/?id=ml_d82f36ae"></script>
    <!--contact js-->
    <script src="<?php echo base_url('');?>style/js/contact.js"></script>
    <script src="<?php echo base_url('');?>style/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/jquery.form.js"></script>
    <script src="<?php echo base_url('');?>style/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url('');?>style/js/mail-script.js"></script>
    <script src="<?php echo base_url().'theme/js/jquery.dataTables.min.js'?>"></script>
            <script src="<?php echo base_url().'theme/js/dataTables.bootstrap4.min.js'?>"></script>
            <script>
              $(document).ready(function() {
                $('#display').DataTable();
              });
            </script>
    <script src="<?php echo base_url('');?>style/js/main.js"></script>

</body>

</html>
<?php
        function limit_words($string, $word_limit){
            $words = explode(" ",$string);
            return implode(" ",array_splice($words,0,$word_limit));
        }
    ?>
     <link rel="stylesheet" href="<?php echo base_url('');?>style/css/magzin.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<style>

* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 100%;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  
  font-weight: bold;
  font-size: 18px;
  
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 48px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  font-weight: 300;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: transparent;
}

/* Fading animation */
.fade {
  animation-name: ;
  animation-duration: 1.5s;
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 4}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>
<div class="slideshow-container">
<?php foreach ($slide->result() as $row) : ?>
<div class="mySlides">
  <div class="numbertext fade">1 / 3</div>
  <img src="<?php echo base_url().'assets/images/'.$row->slide_photo;?>" style="width:100%; height: 500px;">
  <div class="text section_title text-center"><h3><?php echo $row->slide_judul;?></h3></div>
</div>
<?php endforeach;?>
<a class="prev" onclick="plusSlides(-1)">❮</a>
<a class="next" onclick="plusSlides(1)">❯</a>

</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
</div>
   <!-- Team Start -->
        <div class="team">
            <div class="container">
                <div class="section-header text-center">
                    <h2>Kepala Dinas Dan Sekretaris</h2>
                </div>
                <div class="row"><?php foreach ($guru->result() as $row) : ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="team-item">
                            <div class="team-img" height="250px">
                                <a href="<?php echo site_url('pegawai/dtlp/'.$row->guru_id);?>"><img src="<?php echo base_url().'assets/images/'.$row->guru_photo;?>" alt="<?php echo $row->guru_nama;?>"></a>
                            </div>
                            <div class="team-text">
                                <a href="<?php echo site_url('pegawai/dtlp/'.$row->guru_id);?>"><h2><?php echo $row->guru_nama;?></h2></a>
                                <p><?php echo $row->guru_mapel;?></p>
                                <!-- <div class="team-social">
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                        <?php endforeach;?>
                    
                </div>
            </div>
        </div>
        <!-- Team End -->
 <div class="container-fluid bg-light">
        <div class="container">
            <div class="row"><?php foreach ($baner->result() as $row) : ?>
                
                <div class="spread" >
  <div class="spread__left" >
    <div class="title">
     
      <h1 class="medium"><?php echo $row->baner_judul;?></h1>
      
    </div>
    <div class="content">
      <div class="fr" lang="fr">
        <h2><?php echo $row->baner_heading;?></h2>
        <p style="text-align:justify;"><?php echo $row->baner_isi;?></p>
      </div>
      
    </div>
    <div class="spread__page-bottom"><marquee behavior="scroll" direction="right"><i class="fa fa-bicycle fa-4x"></marquee>
      <div class="num"><img src="<?php echo base_url('');?>style/img/halmah.png" alt="" height="65%" width="100%"></div>
    </div>
  </div>&nbsp;

  <div class="spread__right" style="background-image: url('<?php echo base_url().'assets/images/'.$row->baner_photo;?>'); height: 600px;">
    
  </div>&nbsp;
</div>
            <?php endforeach;?>    
            </div>
        </div>

    </div>
    
    <!-- Features End -->
<div class="container pt-5" id="Berita">
        <div class="d-flex flex-column text-center mb-5">
            <h4 class="text-secondary mb-3"></h4>
            <h1 class="display-4 m-0"><span class="text-primary">Berita BKSDA Kab</span>&nbsp;Halmahera Timur</h1>
        </div>
        <div class="row pb-3">
             <?php  foreach ($berita->result() as $row) :?>
            <div class="col-lg-4 mb-4">
                <div class="card border-0 mb-2"><a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>">
                    <img class="card-img-top" src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" alt="<?php echo $row->tulisan_judul;?>" height="230px" width="100%"></a>
                    <div class="card-body bg-light p-4">
                        <h4 class="card-title text-truncate"><a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>">
                                <h4><?php echo $row->tulisan_judul;?></h4>
                            </a></h4>
                        <div class="d-flex mb-3">
                            <small class="mr-2"><i class="fa fa-calendar text-muted"></i><?php echo $row->tanggal;?></small>
                            <small class="mr-2"><i class="fa fa-folder text-muted"></i><a href="<?php echo site_url('blog/kategori/'.str_replace(" ","-",$row->tulisan_kategori_nama));?>" class="show"><?php echo $row->tulisan_kategori_nama;?></a></small>
                            <small class="mr-2"><i class="fa fa-comments text-muted"></i><?php echo $row->tulisan_author;?></small>
                        </div>
                        <p><?php echo limit_words($row->tulisan_isi,10).'...';?></p>
                        <a class="tombol" href="<?php echo site_url('blog/'.$row->tulisan_slug);?>">Read More</a>
                    </div>
                </div>
            </div>
           
        <?php endforeach;?></div>
    </div>
    <!--/ service_area_start  -->

    <!-- popular_program_area_start  -->
    <div class="popular_program_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section_title text-center">
                        <h3>Pengumuman BKSDA</h3>
                    </div>
                </div>
            </div>
            
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row">
                        
                        
                
                
                        <?php foreach ($pengumuman->result() as $row) :?>
                        <div class="col-lg-4 col-md-6">
                            <div class="single__program">
                                <div class="program_thumb">
                                    <img src="img/program/1.png" alt="">
                                </div>
                                <div class="program__content">
                                    <span><?php echo $row->tanggal;?></span>
                                    <h4><a href="<?php echo site_url('pengumuman');?>"><?php echo $row->pengumuman_judul;?></a></h4>
                                    <p><?php echo limit_words($row->pengumuman_deskripsi,10).'...';?></p>
                                    <a href="<?php echo site_url('pengumuman');?>" class="boxed-btn5">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                        <?php endforeach;?>
                        
                        
                        
                    </div>
                </div>
                
                
                
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="course_all_btn text-center">
                        <a href="<?php echo site_url('pengumuman');?>" class="boxed-btn4">Lihat Semua Pengumuman</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- popular_program_area_end -->

    <!-- latest_coures_area_start  -->
    
    <!--/ latest_coures_area_end -->

    <!-- recent_event_area_strat  -->
    <div class="recent_event_area section__padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-10">
                    <div class="section_title text-center mb-70">
                        <h3 class="mb-45">Agenda BKSDA</h3>
                        
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    
                    
                    <?php foreach ($agenda->result() as $row):?>
                    <div class="single_event d-flex align-items-center">
                        <div class="date text-center">
                            <span><?php echo date("d", strtotime($row->agenda_tanggal));?></span>
                            <p><?php echo date("M. y", strtotime($row->agenda_tanggal));?></p>
                        </div>
                        <div class="event_info">
                            <a href="<?php echo site_url('agenda');?>">
                                <h4><?php echo $row->agenda_nama;?></h4>
                             </a>
                            <p><?php echo limit_words($row->agenda_deskripsi,10).'...';?></p>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
    <script>
let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>